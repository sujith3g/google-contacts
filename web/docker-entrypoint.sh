#!/usr/bin/env sh
set -eu

envsubst '${API_URL} ${GOOGLE_CLIENT_ID}' < /config.js.template > /usr/share/nginx/html/config.js

exec "$@"
