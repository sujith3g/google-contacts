import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  withRouter,
  // RouteComponentProps,
} from 'react-router-dom'

import './index.css'
import './flex.css'
import App from './App'
import store from './store'
import * as serviceWorker from './serviceWorker'

export const AppComponent = props => {
  if (!window['reactRouterHistory']) {
    window['reactRouterHistory'] = props.history
  }
  return <App {...props} />
}

export const RouterWrappedApp = withRouter(AppComponent)
export const StateWrappedApp = (
  <Provider store={store}>
    <Router>
      <RouterWrappedApp />
    </Router>
  </Provider>
)

ReactDOM.render(StateWrappedApp, document.getElementById('root'))
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
