import React from 'react'
import { PageHeader } from 'antd'
import { LoginOutlined } from '@ant-design/icons'
import './Header.css'

export default class Header extends React.Component {
  render() {
    return (
      <div className="Header">
        <PageHeader
          onBack={() => null}
          onClick={this.handleClick}
          title={
            <a href="/" style={{ color: 'white' }}>
              Google Contacts
            </a>
          }
          backIcon={false}
          extra={
            <LoginOutlined
              className="logout-btn"
              style={{ color: 'white', width: '32px' }}
            />
          }
        />
      </div>
    )
  }
}
