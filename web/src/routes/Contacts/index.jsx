import React from 'react'
import { Layout } from 'antd'
import { connect } from 'react-redux'

import Header from './Header'
import { actionTypes } from '../../models/actions'
import ContactsList from '../../components/ContactsList'
// import dummyData from './data'

const { Content } = Layout

class Contacts extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    console.log(this.props)
    this.props.dispatch({ type: actionTypes.CONTACTS_FETCH })
  }
  render() {
    const { loading, contacts, total } = this.props
    return (
      <Layout>
        <Header />
        <Layout>
          <Content style={{ padding: '10px 80px' }}>
            <ContactsList loading={loading} list={contacts} total={total} />
          </Content>
        </Layout>
      </Layout>
    )
  }
}
function mapStateToProps(state) {
  return {
    loading: state.contacts.loading,
    contacts: state.contacts.list,
    total: state.contacts.total,
  }
}
export default connect(mapStateToProps)(Contacts)
