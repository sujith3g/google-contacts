const dummyData = [
  {
    resourceName: 'people/c7817127541650226265',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgwxbXdDMWxtQ3dlND0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c7c04150bfbfc59',
          },
        },
        displayName: 'KUCC',
        givenName: 'KUCC',
        displayNameLastFirst: 'KUCC',
        unstructuredName: 'KUCC',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c7c04150bfbfc59',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY-GkdEo08M0LlNOf3_xXLIwCLcDEAEiGQoBSxD___________8BGJuvhfj______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c7c04150bfbfc59',
          },
        },
        value: '047123863477',
        type: 'other',
        formattedType: 'Other',
      },
    ],
  },
  {
    resourceName: 'people/c6464463632936564856',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgwzbmk5SExuVXRyQT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '59b6636c889d7878',
          },
        },
        displayName: 'Pramod Dubai',
        familyName: 'Dubai',
        givenName: 'Pramod',
        displayNameLastFirst: 'Dubai, Pramod',
        unstructuredName: 'Pramod Dubai',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '59b6636c889d7878',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYd5wHeaAwTQ1BqO6Pdo874ACLcDEAEiGQoBUBD___________8BGIzs_P3______wE/s100/photo.jpg',
        default: true,
      },
    ],
    emailAddresses: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2156498f0acac1dc',
          },
        },
        value: 'testmail@sample.com',
        canonicalForm: '+914942638431',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '59b6636c889d7878',
          },
        },
        value: '+971508513198',
        canonicalForm: '+971508513198',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c2402188329958949340',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgwzbmk5SExuVXRyQT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2156498f0acac1dc',
          },
        },
        displayName: 'Vineesh',
        givenName: 'Vineesh',
        displayNameLastFirst: 'Vineesh',
        unstructuredName: 'Vineesh',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2156498f0acac1dc',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYKP9hlpBpbebGTS7NQ_EkCgCLcDEAEiGQoBVhD___________8BGLyPrf3______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2156498f0acac1dc',
          },
        },
        value: '+914942638431',
        canonicalForm: '+914942638431',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c5115611636650254003',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgwvcEprblBnWWo0ND0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '46fe4dae0b649eb3',
          },
        },
        displayName: 'SMS Bal',
        familyName: 'Bal',
        givenName: 'SMS',
        displayNameLastFirst: 'Bal, SMS',
        unstructuredName: 'SMS Bal',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '46fe4dae0b649eb3',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY1pHxI1PGH2eJ8CTb6D33MQCLcDEAEiGQoBUxD___________8BGJ7Szfn______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '46fe4dae0b649eb3',
          },
        },
        value: '*124*3#',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7923808430401063244',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgxjbXlHK0ZpUGVpOD0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6df705c90a4d094c',
          },
        },
        displayName: 'Customer Care',
        givenName: 'Customer Care',
        displayNameLastFirst: 'Customer Care',
        unstructuredName: 'Customer Care',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6df705c90a4d094c',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYaBRKSlOZOmwfye07RyqdUQCLcDEAEiGQoBQxD___________8BGLi-ovv______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6df705c90a4d094c',
          },
        },
        value: '1503',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7433618869374869184',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgwremVJakQ2SW9vVT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '672984fa08badec0',
          },
        },
        displayName: 'Asianet Bill',
        familyName: 'Bill',
        givenName: 'Asianet',
        displayNameLastFirst: 'Bill, Asianet',
        unstructuredName: 'Asianet Bill',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '672984fa08badec0',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYf5a-35bHGq4VnPjw3yOObQCLcDEAEiGQoBQRD___________8BGJyh4vv______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '672984fa08badec0',
          },
        },
        value: '+914713208403',
        canonicalForm: '+914713208403',
        type: 'mobile',
        formattedType: 'Mobile',
      },
      {
        metadata: {
          source: {
            type: 'CONTACT',
            id: '672984fa08badec0',
          },
        },
        value: '+914846616900',
        canonicalForm: '+914846616900',
        type: 'work',
        formattedType: 'Work',
      },
    ],
  },
  {
    resourceName: 'people/c7520162204866516617',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgxtbURoNWlidXVCaz0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '685cfbae8a631689',
          },
        },
        displayName: 'Vodafone',
        givenName: 'Vodafone',
        displayNameLastFirst: 'Vodafone',
        unstructuredName: 'Vodafone',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '685cfbae8a631689',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYGOGH0effgdQ3SQg5SvDpkgCLcDEAEiGQoBVhD___________8BGMDX8fr______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '685cfbae8a631689',
          },
        },
        value: '+911408946800',
        canonicalForm: '+911408946800',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c8827200581469971419',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '7a8084290ba3a3db',
          },
        },
        displayName: 'Using',
        givenName: 'Using',
        displayNameLastFirst: 'Using',
        unstructuredName: 'Using',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '7a8084290ba3a3db',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY9TsUcjlNBE3-A0Fau5gVGQCLcDEAEiGQoBVRD___________8BGPqAsf_______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '7a8084290ba3a3db',
          },
        },
        value: '092-702-689',
        canonicalForm: '+97192702689',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7896470063794424136',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6d95e5af8bf90948',
          },
        },
        displayName: 'Shibu S S Mu',
        familyName: 'Mu',
        givenName: 'Shibu S',
        middleName: 'S',
        displayNameLastFirst: 'Mu, Shibu S S',
        unstructuredName: 'Shibu S S Mu',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6d95e5af8bf90948',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY1pHxI1PGH2eJ8CTb6D33MQCLcDEAEiGQoBUxD___________8BGJ7Szfn______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6d95e5af8bf90948',
          },
        },
        value: '049-421-17249',
        canonicalForm: '+914942117249',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7794123872910944551',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c2a4a5e0c3fa927',
          },
        },
        displayName: 'Changan',
        givenName: 'Changan',
        displayNameLastFirst: 'Changan',
        unstructuredName: 'Changan',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c2a4a5e0c3fa927',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYpuul2XLuuByXhw3C5BWHNQCLcDEAEiGQoBQxD___________8BGMDX8fr______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6c2a4a5e0c3fa927',
          },
        },
        value: '049-426-38185',
        canonicalForm: '+914942638185',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7777683343676503672',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6befe1cb0b4f7678',
          },
        },
        displayName: 'Sija Medics',
        familyName: 'Medics',
        givenName: 'Sija',
        displayNameLastFirst: 'Medics, Sija',
        unstructuredName: 'Sija Medics',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6befe1cb0b4f7678',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY1pHxI1PGH2eJ8CTb6D33MQCLcDEAEiGQoBUxD___________8BGJ7Szfn______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '6befe1cb0b4f7678',
          },
        },
        value: '049-426-44224',
        canonicalForm: '+914942644224',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c7531174930474477648',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '68841bb288422850',
          },
        },
        displayName: 'Dr.IBRAHIM',
        familyName: 'IBRAHIM',
        givenName: 'Dr',
        displayNameLastFirst: 'IBRAHIM, Dr',
        unstructuredName: 'Dr.IBRAHIM',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '68841bb288422850',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYRWswR7jFonxZ255G4D0M_ACLcDEAEiGQoBRBD___________8BGNuwiP7______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '68841bb288422850',
          },
        },
        value: '049-426-45806',
        canonicalForm: '+914942645806',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c6874436641642055122',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '5f66e7b4080105d2',
          },
        },
        displayName: 'Fathima Tchr',
        familyName: 'Tchr',
        givenName: 'Fathima',
        displayNameLastFirst: 'Tchr, Fathima',
        unstructuredName: 'Fathima Tchr',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '5f66e7b4080105d2',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYgtbCXrL8a4HsVc8VS2WWcwCLcDEAEiGQoBRhD___________8BGLyPrf3______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '5f66e7b4080105d2',
          },
        },
        value: '049-426-44258',
        canonicalForm: '+914942644258',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c5814113457336575826',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '50afe1628c5d6752',
          },
        },
        displayName: 'Universal',
        givenName: 'Universal',
        displayNameLastFirst: 'Universal',
        unstructuredName: 'Universal',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '50afe1628c5d6752',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYcGVR-FDXJ4rKaSi-ikIzmgCLcDEAEiGQoBVRD___________8BGNGRivj______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '50afe1628c5d6752',
          },
        },
        value: '048-327-43657',
        canonicalForm: '+914832743657',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c5627897569607285290',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '4e1a4f058cb9fa2a',
          },
        },
        displayName: 'Susheela',
        givenName: 'Susheela',
        displayNameLastFirst: 'Susheela',
        unstructuredName: 'Susheela',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '4e1a4f058cb9fa2a',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYt_NngTbShGvad9E8xkbmmQCLcDEAEiGQoBUxD___________8BGKevgvj______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '4e1a4f058cb9fa2a',
          },
        },
        value: '049-426-22079',
        canonicalForm: '+914942622079',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c4780375246510392550',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '42574df58d5284e6',
          },
        },
        displayName: 'Sumathi Klary',
        familyName: 'Klary',
        givenName: 'Sumathi',
        displayNameLastFirst: 'Klary, Sumathi',
        unstructuredName: 'Sumathi Klary',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '42574df58d5284e6',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmY1pHxI1PGH2eJ8CTb6D33MQCLcDEAEiGQoBUxD___________8BGJ7Szfn______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '42574df58d5284e6',
          },
        },
        value: '049-424-96540',
        canonicalForm: '+914942496540',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c4208406394367680233',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '3a6743518ec49ee9',
          },
        },
        displayName: 'Vlvnr',
        givenName: 'Vlvnr',
        displayNameLastFirst: 'Vlvnr',
        unstructuredName: 'Vlvnr',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '3a6743518ec49ee9',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYUXKfnx3Qrg8HOZxoYGEr7QCLcDEAEiGQoBVhD___________8BGJ6i0f_______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '3a6743518ec49ee9',
          },
        },
        value: '049-425-49790',
        canonicalForm: '+914942549790',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c3303751694751052882',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2dd948a98fbb3452',
          },
        },
        displayName: 'Asianet Care',
        givenName: 'Asianet Care',
        displayNameLastFirst: 'Asianet Care',
        unstructuredName: 'Asianet Care',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2dd948a98fbb3452',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYAp2p_Gru2GDD5qNFKgOOLwCLcDEAEiGQoBQRD___________8BGKevgvj______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2dd948a98fbb3452',
          },
        },
        value: '04712575333',
        canonicalForm: '+914712575333',
        type: 'other',
        formattedType: 'Other',
      },
    ],
  },
  {
    resourceName: 'people/c2468209740428026128',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2240d7ae895f2110',
          },
        },
        displayName: 'University B.tech Section',
        familyName: 'Section',
        givenName: 'University B.',
        middleName: 'tech',
        displayNameLastFirst: 'Section, University B. tech',
        unstructuredName: 'University B.tech Section',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2240d7ae895f2110',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYSVN95fEv1grpLXh9l0aAQACLcDEAEiGQoBVRD___________8BGOS0lfr______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '2240d7ae895f2110',
          },
        },
        value: '04712386332',
        canonicalForm: '+914712386332',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
  {
    resourceName: 'people/c2431423395029069646',
    etag: '%EgoBAj0DCT4LPzcuGgQBAgUHIgw3SVllNnoxS2s3TT0=',
    names: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '21be26b20f482f4e',
          },
        },
        displayName: 'BSNL Tirur',
        familyName: 'Tirur',
        givenName: 'BSNL',
        displayNameLastFirst: 'Tirur, BSNL',
        unstructuredName: 'BSNL Tirur',
      },
    ],
    photos: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '21be26b20f482f4e',
          },
        },
        url:
          'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/V8BNOaftJmYjui0xn9NuOuoGIRrx85_PwCLcDEAEiGQoBQhD___________8BGNGRivj______wE/s100/photo.jpg',
        default: true,
      },
    ],
    phoneNumbers: [
      {
        metadata: {
          primary: true,
          source: {
            type: 'CONTACT',
            id: '21be26b20f482f4e',
          },
        },
        value: '0494 242 1310',
        canonicalForm: '+914942421310',
        type: 'mobile',
        formattedType: 'Mobile',
      },
    ],
  },
]

export default dummyData
