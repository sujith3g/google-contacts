const config = {
  API_URL: `${window.API_URL}`,
  GOOGLE_CLIENT_ID: `${window.GOOGLE_CLIENT_ID}`,
}
export default config
