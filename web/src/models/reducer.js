import { combineReducers } from 'redux'
import { actionTypes } from './actions'

const initialState = {
  token: null,
  currentUser: {},
  loggingIn: false,
  loggingOut: false,
  error: null,
}

const contactsInitState = {
  list: [],
  total: 0,
  loading: false,
  error: null,
}
const authReducer = function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.AUTH_LOGGING_IN:
      return { ...state, loggingIn: action.payload }
    case actionTypes.AUTH_SUCCESS:
      return { ...state, token: action.payload }
    case actionTypes.AUTH_FAILURE:
      return { ...state, error: action.payload }
    default:
      return state
  }
}

const contactsReducer = function (state = contactsInitState, action) {
  switch (action.type) {
    case actionTypes.CONTACTS_FETCHING:
      return { ...state, loading: action.payload }
    case actionTypes.CONTACTS_SUCCESS:
      return {
        ...state,
        list: action.payload.contacts,
        total: action.payload.totalItems,
        loading: false,
      }
    case actionTypes.CONTACTS_FAILURE:
      return { ...state, error: action.payload }
    default:
      return state
  }
}

const defaultReducers = {
  auth: authReducer,
  contacts: contactsReducer,
}

export default combineReducers(defaultReducers)
