import { put, takeLatest, all, select, call } from 'redux-saga/effects'

import { actionTypes } from './actions'
import config from '../config/global.env'

function* authorize({ payload: { accessToken, refreshToken } }) {
  yield put({ type: actionTypes.AUTH_LOGGING_IN, payload: true })
  const tokenBlob = new Blob(
    [
      JSON.stringify(
        {
          access_token: accessToken,
          refresh_token: refreshToken,
        },
        null,
        2
      ),
    ],
    { type: 'application/json' }
  )
  const options = {
    method: 'POST',
    body: tokenBlob,
    mode: 'cors',
    cache: 'default',
  }
  const AUTH_API = `${config.API_URL}/auth/google`
  try {
    const response = yield fetch(AUTH_API, options)
    const token = response.headers.get('x-auth-token')
    console.log(response.data)
    yield put({ type: actionTypes.AUTH_SUCCESS, payload: token })
    if (window.localStorage) {
      window.localStorage.setItem('token', token)
    }
  } catch (err) {
    const msg = err.toString()
    yield put({ type: actionTypes.AUTH_FAILURE, payload: msg })
    if (window.localStorage) {
      window.localStorage.removeItem('token')
    }
  }
}
function* fetchContacts() {
  yield put({ type: actionTypes.CONTACTS_FETCHING, payload: true })
  const state = yield select()
  const { token } = state.auth
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `bearer ${token}`,
    },
  }
  const CONTACTS_API = `${config.API_URL}/contacts/all`
  try {
    const contactsResponse = yield fetch(CONTACTS_API, options)
    const contactsJson = yield call([contactsResponse, 'json'])
    // console.log({ contactsJson })
    const contacts = contactsJson.connections
    const totalItems = contactsJson.totalItems
    yield put({
      type: actionTypes.CONTACTS_SUCCESS,
      payload: { contacts, totalItems },
    })
  } catch (fetchContactsError) {
    const msg = fetchContactsError.toString()
    yield put({ type: actionTypes.CONTACTS_FAILURE, payload: msg })
    console.log(fetchContactsError)
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(actionTypes.AUTH_LOGIN, authorize),
    takeLatest(actionTypes.CONTACTS_FETCH, fetchContacts),
  ])
}
