import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { GoogleLogin } from 'react-google-login'
import config from '../../config/global.env'
import { authorize } from '../../models/actions'

const CLIENT_ID = config.GOOGLE_CLIENT_ID

class GoogleButton extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isLogined: false,
      user: null,
      accessToken: '',
    }
  }

  login = response => {
    if (response.accessToken) {
      const { accessToken, refreshToken } = response
      this.props.dispatch(authorize({ accessToken, refreshToken }))
      console.log({ response })
    }
  }
  handleLoginFailure = response => {
    alert('Failed to log in')
  }

  render() {
    const { token } = this.props
    if (typeof token === 'string') {
      return (
        <Redirect to={{ pathname: '/contacts', state: { from: '/login' } }} />
      )
    }
    return (
      <div>
        <GoogleLogin
          clientId={CLIENT_ID}
          buttonText="Login"
          scope={
            'profile email https://www.googleapis.com/auth/contacts.readonly'
          }
          onSuccess={this.login}
          onFailure={this.handleLoginFailure}
          cookiePolicy={'single_host_origin'}
          responseType="code,token"
          prompt="consent"
        />
      </div>
    )
  }
}
function mapStateToProps(state) {
  return { token: state.auth.token }
}

export default connect(mapStateToProps)(GoogleButton)
