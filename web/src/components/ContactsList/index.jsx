import React from 'react'
import { get } from 'lodash'
import { Row, Col, List, Skeleton, Avatar } from 'antd'

import './index.css'
export default class ContactsList extends React.Component {
  render() {
    const { loading, list, total } = this.props
    return (
      <div>
        <div>
          <h1> Contacts ({total}) </h1>
        </div>
        <div className="ContactsList">
          <List
            itemLayout="horizontal"
            size="large"
            loading={loading}
            dataSource={list}
            header={
              <Row>
                <Col span={8}>Name</Col>
                <Col span={8}>Email</Col>
                <Col span={8}>Phone number</Col>
              </Row>
            }
            renderItem={item => (
              <div className="contacts-item">
                <List.Item key={item.resourceName}>
                  <Skeleton avatar title={false} loading={item.loading} active>
                    <List.Item.Meta
                      avatar={<Avatar src={get(item, 'photos[0].url', '')} />}
                    />
                    <div className="contact-list-item">
                      <Row>
                        <Col span={8}>
                          <h1> {item.names[0].displayName}</h1>
                        </Col>
                        <Col span={8}>
                          <h1>{get(item, 'emailAddresses[0].value', '')}</h1>
                        </Col>
                        <Col span={8}>
                          <h1>{get(item, 'phoneNumbers[0].value', '')}</h1>
                        </Col>
                      </Row>
                    </div>
                  </Skeleton>
                </List.Item>
              </div>
            )}
          />
        </div>
      </div>
    )
  }
}
