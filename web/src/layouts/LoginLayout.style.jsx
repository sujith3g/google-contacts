import styled from 'styled-components'

export const FullSizeDiv = styled.div`
  width: 100vw;
  height: 100vh;
`

export const CenteredDiv = styled.div`
  max-width: max-content;
  margin: auto;
`
