import React from 'react'

import GoogleButton from '../components/GoogleButton'
import { FullSizeDiv, CenteredDiv } from './LoginLayout.style.jsx'

export default class AppLayout extends React.Component {
  constructor() {
    super()
    this.state = {}
  }
  render() {
    return (
      <FullSizeDiv className="vertical layout center-jusitfied">
        <CenteredDiv>
          <GoogleButton {...this.props} />
        </CenteredDiv>
      </FullSizeDiv>
    )
  }
}
