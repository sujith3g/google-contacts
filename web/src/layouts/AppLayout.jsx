import React from 'react'
import { Layout } from 'antd'

import Header from './Header'
const { Content } = Layout

export default class AppLayout extends React.Component {
  constructor() {
    super()
    this.state = {}
  }
  render() {
    return (
      <Layout>
        <Header />
        <Layout>
          <Content style={{ padding: '10px 80px' }}>
            Contact list comes here
          </Content>
        </Layout>
      </Layout>
    )
  }
}
