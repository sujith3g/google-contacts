import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Contacts from './routes/Contacts'
import LoginLayout from './layouts/LoginLayout'

import './App.css'

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Route
          path="/login"
          render={props => <LoginLayout {...props} />}
        ></Route>
        <Route
          path="/contacts"
          render={props => <Contacts {...props} />}
        ></Route>
      </Router>
    )
  }
}
