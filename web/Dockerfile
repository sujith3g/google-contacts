# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:10.16.0 as build-stage
RUN npm i -g yarn
WORKDIR /app
ADD package.json yarn.lock /app/
RUN yarn --frozen-lockfile
COPY ./ /app/
RUN sh -c "yarn build"

# Stage 1, based on Nginx, to have only the compiled app, ready for
FROM nginx:stable
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/nginx-default.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/build /usr/share/nginx/html
COPY --from=build-stage /app/config.js.template /
COPY --from=build-stage /app/docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
