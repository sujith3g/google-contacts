# Google Contacts


An app using [react](https://reactjs.org/), [antd](https://ant.design/),
and [expressjs](https://expressjs.com/) for listing Google contacts of
the logged in user. It
uses OAuth APIs of google, and [googleapis](https://www.npmjs.com/package/googleapis).
This app uses [Create React App](https://create-react-app.dev/) for the frontend and express.js for
the backend/API.

## Design

On successfull login/consent from a google user,
frontend app sends users's google access / refresh token to the backend
as a post request, On the
backend passport google token strategy is used to validate the access to
ken. Once access token is validated the details gets upserted to the mongodb and JWT token is send back to
front-end. All subsequent requests(like /conntacts/all to get contacts)
validated/authenticated with JWT token, and uses the access/refresh
token stored earlier for getting the contacts data using googleapis.

## Setup

First install [Node.JS v8.x LTS or higher](https://nodejs.org/en/download/). and optionally install `yarn` using `npm i -g yarn`.

## Running

#### Backend or API

```bash 
cd api/
# install dependencies
yarn
   or
npm istall
# start the app
yarn start
   or 
npm start

# start the app in development mode
yarn run dev

```

#### Frontend

```
cd web/
# install dependencies
yarn
   or
npm install
# start the app
yarn start
   or
npm start

```



### Docker build and run

#### Backend or API

- Build image

```
cd api/
# build image
docker build -t google-contacts-api:latest .
```

- Run the container

```
docker run -d -p 3030:3030 -e MONGO_DB_URI="your mongo URL" \
            -e GOOGLE_CLIENT_ID="Your Google client ID" \
            -e GOOGLE_CLIENT_SECRET="Your google client secret" \
            -e JWT_SECRET="Your jwt secret" \
            google-contacts-api:latest

```

#### Frontend

- Build image

```
cd web/
# build image
docker build -t google-contacts:latest .
```

- Run the container

```
docker run -d -p 3000:80 -e API_URL=http://localhost:3030 \
    -e GOOGLE_CLIENT_ID="Your google client ID" \
    google-contacts:latest
```






