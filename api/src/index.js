import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';

import config from './config';
import infoRoute from './routes/info.route';
import authRoute from './routes/auth.route';
import contactsRoute from './routes/contacts.route';
import connectToDb from './db/connect';

import './services/passport';

const port = config.port;

connectToDb();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
const corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token'],
};
app.use(cors(corsOption));

app.use('/info', infoRoute);
app.use('/auth', authRoute);
app.use('/contacts', contactsRoute);

const server = app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log('app running on port.', server.address().port);
});
