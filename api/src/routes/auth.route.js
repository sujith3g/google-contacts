import express from 'express';
import passport from 'passport';

import { generateToken, sendToken } from '../utils/token.utils';

const router = express.Router();

router.post(
  '/google',
  passport.authenticate('google-token', { session: false }),
  (req, res, next) => {
    console.log('@ POST /auth/google');
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }
    req.auth = {
      id: req.user.id,
    };

    next();
  },
  generateToken,
  sendToken
);

export default router;
