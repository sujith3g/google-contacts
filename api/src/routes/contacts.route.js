import express from 'express';
import { google } from 'googleapis';
import config from '../config';
import { authenticateToken } from '../utils/token.utils';
import User from '../models/user.model';

const router = express.Router();

const SCOPES = ['https://www.googleapis.com/auth/contacts.readonly'];

router.get('/all', authenticateToken, async (req, res) => {
  // console.log({ reqUser: req.user });
  const user = await User.findOne({ _id: req.user.id });
  // console.log({ user });
  const oAuth2Client = new google.auth.OAuth2(
    config.google.CLIENT_ID,
    config.google.CLIENT_SECRET,
    '/auth/google/redirect'
  );
  const { token, refreshToken } = user.googleProvider;
  oAuth2Client.setCredentials({ access_token: token, refresh_token: refreshToken });
  const service = google.people({ version: 'v1', auth: oAuth2Client });
  let contacts = [];
  try {
    const connection = await service.people.connections.list({
      resourceName: 'people/me',
      pageSize: 20,
      personFields: 'names,emailAddresses,photos,phoneNumbers',
    });
    contacts = connection.data;
    // console.log({ connections: connection.data });
  } catch (err) {
    console.log(err);
  }
  res.json(contacts);
});

export default router;
