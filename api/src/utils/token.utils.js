import jwt from 'jsonwebtoken';

import config from '../config';

const createToken = function (auth) {
  return jwt.sign(
    {
      id: auth.id,
    },
    config.jwt.SECRET,
    {
      expiresIn: 60 * 120,
    }
  );
};

export const generateToken = function (req, res, next) {
  req.token = createToken(req.auth);
  return next();
};
export const sendToken = function (req, res) {
  res.setHeader('x-auth-token', req.token);
  return res.status(200).send(JSON.stringify(req.user));
};
export const authenticateToken = function (req, res, next) {
  // Gather the jwt access token from the request header
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return res.sendStatus(401); // if there isn't any token

  jwt.verify(token, config.jwt.SECRET, (err, user) => {
    console.log(err);
    if (err) return res.sendStatus(403);
    req.user = user;
    next(); // pass the execution off to whatever request the client intended
  });
};
