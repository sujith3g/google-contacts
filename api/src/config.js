import 'dotenv/config';

const config = {
  port: process.env.PORT || 3030,
  mongodb: {
    DB_URI: process.env.MONGO_DB_URI,
  },
  google: {
    CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
  },
  jwt: {
    SECRET: process.env.JWT_SECRET || 'my-secret',
  },
};

export default config;
