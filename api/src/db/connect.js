import Mongoose from 'mongoose';
import config from '../config';

Mongoose.Promise = global.Promise;

const connectToDb = async () => {
  let dbUri = config.mongodb.DB_URI;
  try {
    await Mongoose.connect(`${dbUri}`, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('Connected to mongo!!!');
  } catch (err) {
    console.log(err);
    console.error('Could not connect to MongoDB');
  }
};

export default connectToDb;
