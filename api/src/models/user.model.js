import mongoose from 'mongoose';

const UserSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      index: true,
      trim: true,
      unique: true,
      match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    name: { type: String, required: true, index: true },
    googleProvider: {
      type: {
        id: String,
        token: String,
      },
    },
  },
  { collection: 'Users' }
);

let UsersModel = mongoose.model('Users', UserSchema);
UsersModel.getAll = () => {
  return UsersModel.find({});
};

export default UsersModel;
