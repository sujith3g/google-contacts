import passport from 'passport';
import * as passportGoogleToken from 'passport-google-token';

import config from '../config';
import User from '../models/user.model';

const GoogleTokenStrategy = passportGoogleToken.Strategy;

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: config.google.CLIENT_ID,
      clientSecret: config.google.CLIENT_SECRET,
    },
    function (accessToken, refreshToken, profile, done) {
      // console.log({ accessToken, refreshToken });
      User.findOne({ 'googleProvider.id': profile.id })
        .then(currentUser => {
          if (!currentUser) {
            new User({
              name: profile.displayName,
              email: profile.emails[0].value,
              googleProvider: {
                id: profile.id,
                token: accessToken,
                refreshToken: refreshToken,
              },
            })
              .save()
              .then(newUser => {
                done(null, newUser);
              })
              .catch(insertUserError => {
                console.log(insertUserError);
                done(insertUserError, null);
              });
          } else {
            User.updateOne({ _id: currentUser.id }, { 'googleProvider.token': accessToken })
              .then(() => {
                done(null, currentUser);
              })
              .catch(userUpdateError => {
                console.log(userUpdateError);
                done(userUpdateError, null);
              });
          }
        })
        .catch(findUserError => {
          console.log(findUserError);
          done(findUserError, null);
        });
    }
  )
);
